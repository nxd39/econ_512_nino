function f= f(theta, X)
amean=mean(X);
gmean=exp(mean(log(X)));
f=exp(psi(theta))/theta-gmean/amean;
end