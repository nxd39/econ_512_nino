function df= df(theta)
df=(exp(psi(theta))*(trigamma(theta)*theta-1))/(theta^2);
end