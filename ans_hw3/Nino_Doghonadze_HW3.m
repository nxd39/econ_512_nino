diary on
display ('Question 1')
clear
clc
display('Answer to a) and b) is in an accompanying pdf file')
display ('Code runs after inserting the data as a numerical column vector "x"')
display ('To test it I used one of the variables from the dataset for the other question')
load 'hw3.mat';
for i=1:601;
     x(i,1)=X(i,2);
 end;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
display('c)')
x0=1;
nmax=1000000;
tol=eps;
fun = @(theta)f(theta, x);
dfun=@(theta)df(theta);
    theta(1) = abs(x0 - (fun(x0)/dfun(x0)));
    ex(1) = abs(theta(1)-x0);
    k = 2;
    while (ex(k-1) >= tol) && (k <= nmax)
        theta(k) = abs(theta(k-1) - (fun(theta(k-1))/dfun(theta(k-1))));
        % you can calculate function and its derivative in one function, it
        % is faster
        ex(k) = abs(theta(k)-theta(k-1));
        k = k+1;
    end
    
    if ex(max(k)-1)<eps
        display('solution found')
        theta1=theta(max(k)-1)
        theta2=theta1/mean(x)
    else display('solution not found')
    end
% recheck for estimate of theta1 with fsolve
x = fsolve(fun,x0)
n=length(x);
A=[n*trigamma(theta1), -n/theta2; -n/theta2, n*theta1/(theta2^2)];
COV=inv(A);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
display('d)')
fraction=1.1:0.1:3;
for i=1:length(fraction);
   f= @(theta)f1(theta, fraction(1, i));
   theta11(1, i)=fsolve(f, x0); 
end
plot (fraction, theta11)
title('\theta_1(Y_2/Y_1)')
xlabel('Y_2/Y_1')
ylabel('\theta_1')
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
% you needed to clear up after your previous section
display ('Question 2')
load 'hw3.mat';
x0=[0, 0, 0, 0, 0, 0];
display ('a)')

display('FMINUNC without a derivative')
options = optimoptions('fminunc','Display','iter');
f = @(theta)loglikelihood(theta, X, y);
[theta, fval, exitflag, output] = fminunc(f,x0, options)

display('FMINUNC without a derivative')
options = optimoptions('fminunc','Display','iter','Algorithm','trust-region','SpecifyObjectiveGradient',true);
f1 = @(theta)loglikelihoodwithgrad(theta, X, y);
[theta1, fval1, exitflag1, output1] = fminunc(f1,x0,options)

display('FMINSEARCH')
x0=[1, 1, 1, 1, 1, 1];
options = optimset('Display','iter');
f2 = @(theta)loglikelihood(theta, X, y);
[theta2, fval2, exitflag2, output2] = fminsearch(f2,x0, options)

display('BHHH')
x0=[0, 0, 0, 0, 0, 0]
options = optimset('GradObj','on','Hessian','user-supplied','Display','iter');
f3 = @(theta)loglikelihoodwithgradhess(theta, X, y);
[theta3, fval3, exitflag3, output3] = fminunc(f3,x0,options)
% this did not seem to converge properly

display('b)')
h0=(X.*(-exp(X*x0')+y))'*(X.*(-exp(X*x0')+y));
e0=eig(h0)
h3=(X.*(-exp(X*theta3')+y))'*(X.*(-exp(X*theta3')+y));
e3=eig(h3)

display('c)')
options = optimset('Display','iter');
f4=@(theta) NLS(theta, X, y);
[theta4, fval4, exitflag4, output4] = fminunc(f4,x0,options)

display('d) Assuming no heteroscedasticity:')
ehat=y-exp(X*theta4');
sigma2=mean(ehat.^2);
A=exp(X*theta4');
for i=1:601;
    for j=1:6;
        g(i, j)=A(i)*X(i, j);    
    end;
end;

display('NLS Standard errors')
G=g'*g;
COV=sigma2*inv(G./601);
SE_matrix=(COV./601)^(0.5);
for i=1:6
    SE(i, 1)=SE_matrix(i, i)
end

display('BHHH Standard errors')
BHHH=h3./601;
SE_BHHH_matrix=(BHHH./601)^(0.5);
for i=1:6
    SE_BHHH(i, 1)=SE_BHHH_matrix(i, i)
end
diary off