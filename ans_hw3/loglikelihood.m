function loglikelihood = loglikelihood(theta, X, Y)
% loglikelihood function that I need to minimize
linear =X*theta';
A=-exp(linear);
B=linear.*Y;
C=factorial(Y);
D=-log(C);
E=A+B+D;
loglikelihood=-sum(E);
end