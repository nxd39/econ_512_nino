function NLS= NLS(theta, X, Y)
% loglikelihood function that I need to minimize
linear =X*theta';
A=-exp(linear);
R=(Y+A).^2
NLS=sum(R);
end