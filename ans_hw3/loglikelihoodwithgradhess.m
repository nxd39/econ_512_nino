function [f, g, h]= loglikelihoodwithgradhess(theta, X, Y)
% loglikelihood function that I need to minimize
linear =X*theta';
A=-exp(linear);
B=linear.*Y;
C=factorial(Y);
D=-log(C);
E=A+B+D;
F=X.*(A+Y);
f=-sum(E);
if nargout > 1
g=-X'*(A+Y);
h=F'*F;    
end