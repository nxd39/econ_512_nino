function [ q0, qa, qb, qc ] = Demand_System(v,p)
% Demand_System as a function of vectors of value for quality and price 
%  v(1) is value for product A, v(2) for product B, etc.
q0= 1/(1+exp(v(1)-p(1))+ exp(v(2)-p(2))+exp(v(3)-p(3)));
qa= exp(v(1)-p(1))/(1+exp(v(1)-p(1))+ exp(v(2)-p(2))+exp(v(3)-p(3)));
qb= exp(v(2)-p(2))/(1+exp(v(1)-p(1))+ exp(v(2)-p(2))+exp(v(3)-p(3)));
qc= exp(v(3)-p(3))/(1+exp(v(1)-p(1))+ exp(v(2)-p(2))+exp(v(3)-p(3)));
end

