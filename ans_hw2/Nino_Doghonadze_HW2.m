<<<<<<< HEAD
% Empirical Methods, Homework 2
% Nino Doghonadze, 09/22/2017
clear
clc
diary on
%% Question 1
%I have created a Demand_System function file and am using that to evaluate
%the function at given argument values.
V=[-1, -1, -1];
p=[1; 1; 1];

display('Question 1')
display('Demand for given parameters is...')
[q0, qa, qb, qc]=Demand_System(V,p);
Demand=[q0, qa, qb, qc]

%% Question 2
%Here I need to solve for the first order conditions of each firm. Hence I
%created another function file with all three "reduced" FOCs to be solved 
%for interior equilibrium. I removed all the multipliers that are 
%definitely positive to ease computation.
display('Question2')
display('Using Broyden method, convergence IS achieved within 10 iterations')
display('For each starting value, default tolerance level of square root of eps')
guess_1=[1; 1; 1];
guess_2=[0; 0; 0];
guess_3=[0; 1; 2];
guess_4=[3; 2; 1];

tic
display('under guess 1')
[x1, fval,flag,it] = broyden(@FOC, guess_1)
toc
tic
display('under guess 2')
[x2,fval, flag,it] = broyden(@FOC, guess_2)
toc
tic
display('under guess 3')
[x3,fval, flag,it] = broyden(@FOC, guess_3)
toc
tic
display('under guess 4')
[x4,fval,flag,it] = broyden(@FOC, guess_4)
toc

Broyden_Prices=[x1, x2, x3, x4]
[qstar0, qstar1, qstar2, qstar3] =Demand_System(V, x1);
Broyden_Equilibrium_Quantities=[qstar0, qstar1, qstar2, qstar3]
%% Question 3
% For each price, I use the iteration
display('Question 3')
syms p1 p2 p3;
 FOC1=@(p1, p2, p3)(1+exp(-1-p1)+ exp(-1-p2)+exp(-1-p3))-p1*(1+0*exp(-1-p1)+ exp(-1-p2)+exp(-1-p3));
 FOC2=@(p1, p2, p3)(1+exp(-1-p1)+ exp(-1-p2)+exp(-1-p3))-p2*(1+exp(-1-p1)+0* exp(-1-p2)+exp(-1-p3));
 FOC3=@(p1, p2, p3)(1+exp(-1-p1)+ exp(-1-p2)+exp(-1-p3))-p3*(1+exp(-1-p1)+ exp(-1-p2)+0*exp(-1-p3));
 
 % Initial guesses 

%% GUESS1
pr1=1;
pr2=1;
pr3=1;

%% GUESS2
%  pr1=0;
%  pr2=0;
%  pr3=0;

%% GUESS3 
%  pr1=0;
%  pr2=1;
%  pr3=2;
  
%% GUESS4
%  pr1=3;
%  pr2=2;
%  pr3=1;

%% Choices of point A & B 
% A will be one of the guesses, given in the problem
 p1A=pr1;
 p2A=pr2;
 p3A=pr3;
 
 % My choices for point B
 p1B=p1A+4;
 p2B=p2A+4;
 p3B=p3A+4;
 
 % Preparing the loop
 excess1 = 1;
 excess2 = 1;
 excess3 = 1;
 
 excess=1;
 excessFOC1=1;
 excessFOC2=1;
 excessFOC3=1;
 
 tol=0.01 % you set tolerance too low. In broyden the default tolarence is 1e-8
 maxiter=1000000
 counter1=1;
 tic
 while abs(excess)>tol;
   counter1=counter1+1;
   if counter1>maxiter
    break
end
   excess=max(max(excessFOC1, excessFOC2), max(excessFOC2, excessFOC3));
   while abs(excess1) > tol;  
   yA1=FOC1(p1A, pr2, pr3);
   yB1=FOC1(p1B, pr2, pr3);
   slope1=(yA1-yB1)/(p1A-p1B);
   constant1 = yA1-slope1*p1A;
   p11=-(constant1)/(slope1);
   yS1=FOC1(p11, pr2, pr3);
   excess1=yS1; 
   if p11==p1A;
       break
   end
   if p11==p1B;
       break
   end
      if p1A==p1B
       break       
   end
      if abs(excess1) > tol;
    p1A = p1B;
    p1B = p11;
   end
   end

while abs(excess2) > tol;  
   yA2=FOC2(pr1, p2A, pr3);
   yB2=FOC2(pr1, p2B, pr3);
   slope2=(yA2-yB2)/(p2A-p2B);
   constant2 = yA2-slope2*p2A;
   p22=-(constant2)/(slope2);
   yS2=FOC2(pr1, p22, pr3);
   excess2=yS2; 
   if p22==p2A;
       break
   end
   if p22==p2B;
       break
   end
      if p2A==p2B
       break       
   end
      if abs(excess2) > tol;
    p2A = p2B;
    p2B = p22;
      end
end

while abs(excess3) > tol;  
   yA3=FOC3(pr1, pr2, p3A);
   yB3=FOC3(pr1, pr2, p3B);
   slope3=(yA3-yB3)/(p3A-p3B);
   constant3 = yA3-slope3*p3A;
   p33=-(constant3)/(slope3);
   yS3=FOC3(pr1, pr2, p33);
   excess3=yS3; 
   if p33==p3A;
       break
   end
   if p33==p3B;
       break
   end
   if p3A==p3B
       break       
   end
       if abs(excess3) > tol;
    p3A = p3B;
    p3B = p33;
   
   end
end

excessFOC1=FOC1(p11, p22, p33);
excessFOC2=FOC2(p11, p22, p33);
excessFOC3=FOC3(p11, p22, p33);

if abs(excess)>tol;
pr1=p11;
pr2=p22;
pr3=p33;
end

end
 
if abs(excess)<=tol;
    converged1=1;
else
    converged1=0;
end;
converged1
counter1
if converged1==1
    solution1=[pr1; pr2; pr3]
else
    display('no convergence achieved')
end
display('Here only guess one converged in less than million iterations.')
display('and only roughly, i.e. for as low tolerance level as 0.01.')
toc
display('Broyden has all advantages.')
display('This method depends a lot on starting values.')
display('Does not come very close to the real solution.') 


% because of the bad tolerance the result is incorrect
%% Question 4
display('Question 4')
tic
pStart=guess_1;
%pStart=guess_2;
%pStart=guess_3;
%pStart=guess_4;

diff=1;
qG=[];
count=1;
while abs(diff)>tol
    count=count+1;
    if count>maxiter
        break
=======
function foc = foc(p,v,n,pstatic)
% this function is supposed to calculate vector of excesses in FOC of each
% firm's maximization problem

if nargin>3
   p_use = pstatic;
   p_use(n) = p;
   p = p_use;
end


% calculate market shares given parameters
    Q = q(p,v);
% now throw away outside good market share
    Q = Q(2:end);
    
    
    
    foc_t = p - 1./(1-Q);
%     foc_t = (1-p).*Q + p.*Q.^2;
    
%     These two above FOCs are equivalent mathematically when Q>0, but the
%     first one gives you much netter numerical properties because of the
%     lack of singularity. Homework did not specify which one to use,
%     though
    
% optional argument n
    if nargin==2
        foc = foc_t;
    elseif nargin >2
        foc = foc_t(n);
>>>>>>> 69aea7d82c43e4de287ff1062fc29faa379984f2
    end
end