function [FOC] = FOC(p)
% This is reduced form of FOC. I got rid of all the terms that multiply 
% these expressions and are definitely nonzero.
FOC1=(1+exp(-1-p(1))+ exp(-1-p(2))+exp(-1-p(3)))-p(1)*(1+0*exp(-1-p(1))+ exp(-1-p(2))+exp(-1-p(3)));
FOC2=(1+exp(-1-p(1))+ exp(-1-p(2))+exp(-1-p(3)))-p(2)*(1+exp(-1-p(1))+ 0*exp(-1-p(2))+exp(-1-p(3)));
FOC3=(1+exp(-1-p(1))+ exp(-1-p(2))+exp(-1-p(3)))-p(3)*(1+exp(-1-p(1))+ exp(-1-p(2))+0*exp(-1-p(3)));
FOC=[FOC1; FOC2; FOC3];
end

