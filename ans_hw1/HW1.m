diary EM_HW1
diary on
% Problem 1
X=[1 1.5 3 4 5 7 9 10]
Y1= -2+0.5*X
Y2= -2+0.5*X.^2
%Plotting, version 1
plot (X, Y1, X, Y2)
%Plotting, version 2
plot (X, Y1)
hold on
plot (X, Y2)
hold off

% Problem 2
X=linspace(-10,20,200)
sum(X)

% Problem 3
A=[2 4 6; 1 7 5; 3 12 4]
B=[-2; 3; 10]
C=A'*B
D= inv(A'*A)*B


% this is not the tight way to do what you were asked. It computes the
% different sum
E=sum(A*B)
rows2remove=[2];
cols2remove=[3];
A(rows2remove,:)=[];
A(:,cols2remove)=[];
A
A=[2 4 6; 1 7 5; 3 12 4]

% inv is slow, use \ operator instead, check the answer key
x=inv(A)*B

% Problem 4
Z=zeros(3,3)
% learn to use kron operator, check answer key to see how
B=[A Z Z Z Z; Z A Z Z Z; Z Z A Z Z; Z Z Z A Z; Z Z Z Z A]

% Problem 5
A=normrnd(10,5,[5 3])
A_converted=zeros(5,3);
% There is more efficient way to do so since for loops are very slow in
% matlab. 
for i=1:5;
    for j=1:3;
        if A(i,j)<10;
            A_converted(i, j)=0;
        else A_converted(i,j)=1;
        end;
    end;
end;
A_converted

% Problem 6

% Import data from csv file.

% don't use the absolute path, I have the different ones on my computer.
% just put the data file in the same directory
filename = 'C:\Nino Files\PSU\Year 2\EM\econ_512_2017\homework\datahw1.csv';
delimiter = ',';

formatSpec = '%f%f%f%f%f%f%[^\n\r]';

fileID = fopen(filename,'r');

dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'EmptyValue', NaN,  'ReturnOnError', false);

fclose(fileID);

VarName1 = dataArray{:, 1};
VarName2 = dataArray{:, 2};
VarName3 = dataArray{:, 3};
VarName4 = dataArray{:, 4};
VarName5 = dataArray{:, 5};
VarName6 = dataArray{:, 6};

clearvars filename delimiter formatSpec fileID dataArray ans;

% Prepare matrix for OLS

tbl = table(VarName3, VarName4, VarName6, VarName5, 'VariableNames',{'Export','RD','cap', 'prod'});
% lm = fitlm(tbl,'VarName5~1+VarName3+VarName4+VarName6')
lm = fitlm(tbl,'prod~1+Export+RD+cap')

diary off
        