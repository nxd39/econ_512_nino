function lm4=lm4(par, X, Y, Z)
m=100;
[~, N] = size(X);
%Now there are 6 parameters to be estimated
% 1-gamma; 2-beta0, 3-u0, 4-sigmab, 5-sigmabu, 6-sigmau
mu=[par(2), par(3)];
S=[par(4), par(5); par(5), par(6)];
joint=(mu'.*ones(2, m)+nearestSPD(S)*randn(2, m));
for j=1:N
    jointmonte(:, :, j)=joint(:, :);
end
fevalm=zeros(m, N);
for i=1:m
    fevalm(i, :)=integrand4(jointmonte(:,i, :), par(1), X, Y, Z);
end
integralm=mean(fevalm);
lnintegralm=log(integralm');
lm4=-sum(lnintegralm);
end