function lm=lm(par, X, Y)
m=100;
[~, N] = size(X);
betamonte=(par(1).*ones(1, m)+par(2).*randn(1, m))'*ones(1, N);
fevalm=zeros(m, N);
for i=1:m
    fevalm(i, :)=integrand(betamonte(i,:), X, Y);
end
integralm=mean(fevalm);
lnintegralm=log(integralm');
lm=-sum(lnintegralm);
end