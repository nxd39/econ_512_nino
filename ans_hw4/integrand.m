function [integrand]=integrand(beta, X, Y)
%this function file calculates the individual likelihood function
%joint is 2x100 matrix of beta (1x100 random effect), random stuff.
%X is a 20x100 matrix, data.
%Y is a 20x100 matrix, data.
[T, N] = size(X);
One=ones(T, 1);
Beta=One*beta;
%this will be TxN matrix of individual random effects as rows.
epsilon=Beta.*X;
%this will be the argument of the logistic distribution, TxN;
ONe=ones(T, N);
success=(ONe+exp(-epsilon)).^(-1);
%this will be TxN matrix
failure=ONe-(ONe+exp(-epsilon)).^(-1);
%this will be TxN matrix
term1=success.^Y;
term2=failure.^(ONe-Y);
product=term1.*term2;
lli=prod(product);
%This is a row vector of individual likelihood functions, 1XN
[integrand]=lli;
end
