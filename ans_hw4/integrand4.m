function [integrand4]=integrand4(joint, gamma, X, Y, Z)
%this function file calculates the individual likelihood function
%joint is 2x100 matrix of beta (1x100 random effect) and 
%u(1x100 random error), random stuff.
%X is a 20x100 matrix, data.
%Y is a 20x100 matrix, data.
%Z is a 20x100 matrix, data
%gamma is a scalar parameter.
[T, N] = size(X);
One=ones(T, 1);
beta=joint(1, :);
u=joint(2, :);
Beta=One*beta;
U=One*u;
Gamma=One.*gamma;
%this will be TxN matrix of individual random effects as rows.
epsilon=Beta.*X+ Gamma.*Z+U;
%this will be the argument of the logistic distribution, TxN;
ONe=ones(T, N);
success=(ONe+exp(-epsilon)).^(-1);
%this will be TxN matrix
failure=ONe-(ONe+exp(-epsilon)).^(-1);
%this will be TxN matrix
term1=success.^Y;
term2=failure.^(ONe-Y);
product=term1.*term2;
lli=prod(product);
%This is a row vector of individual likelihood functions, 1XN
[integrand4]=lli;
end
