%Homework 4 
%Nino Doghonadze, December 2017
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Question 1
clear 
clc
load('hw4data.mat', 'data');
N=data.N;
T=data.T;
X=data.X;
Y=data.Y;
Z=data.Z;
beta0=0.1;
sigmab=1;
fun=@(beta)integrand(beta, X, Y);
%specifying the nodes of integration
k=20;
[beta_nodes, weights] = qnwnorm(20, beta0, sigmab);
beta=beta_nodes*ones(1, N);
feval=zeros(k, N);
value=zeros(k, N);
for i=1:k
     feval(i, :)=fun(beta(i,:));
     value(i, :)=feval(i, :).*weights(i, 1);
%      It is better to calculate loglikelihood for each draw and then sum
%      them because likelihood may differ many orders of magnitude, asking
%      for overflow and underflow errors. check with Miranda Fletcher and
%      Judd.
end
integral=sum(value);
Likelihood=prod(integral');
lnintegral=log(integral');
LogLikelihood=sum(lnintegral);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Question 2
m=100;
betamonte=(beta0.*ones(1, m)+sigmab.*randn(1, m))'*ones(1, N);
fevalm=zeros(m, N);
for i=1:m
    fevalm(i, :)=fun(betamonte(i,:));
end
integralm=mean(fevalm);
Likelihoodm=prod(integralm');
lnintegralm=log(integralm');
LogLikelihoodm=sum(lnintegralm);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Question 3
% Using quadrature
lq=@(par)ll(par, X, Y);
parq0=[beta0, sigmab];
A=[0, -1];
b=0;
[parq, llq] = fmincon(lq,parq0,A,b);
% for some reason this parameter vector is just two doubles, not three. 
%Using Monte Carlo
lm=@(par)lm(par, X, Y);
parm0=[beta0, sigmab];
A=[0, -1];
b=0;
[parm, llm] = fmincon(lm,parm0,A,b);
%  this has barely moved from the initial point.
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Question 4
lm4=@(par)lm4(par, X, Y, Z);
parm40=[0, beta0, 0 ,sigmab, 0, 1];
A = [];
b = [];
Aeq = [];
beq = [];
lb=[-inf, -inf, -inf, eps, -inf, eps];
ub=[];
nonlcon = @constraint;
[parm4, llm4] = fmincon(lm4,parm40,A,b,Aeq,beq,lb,ub,nonlcon);
S=[parm4(4),parm4(5); parm4(5), parm4(6)];
%  this does not converge for any maagable time on my computer.
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Question 5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Quadrature Method')
disp('Starting value')
disp(parq0)
disp('ArgMax')
disp(parq)
disp('Maximized LogLikelihood')
disp(llq)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Monte Carlo Method')
disp('Starting value')
disp(parm0)
disp('ArgMax')
disp(parm)
disp('Maximized LogLikelihood')
disp(llm)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Monte Carlo Method, Full model')
disp('Starting value')
disp(parm40)
disp('ArgMax')
disp('beta0')
disp(parm4(2))
disp('u0')
disp(parm4(3))
disp('gamma')
disp(parm4(1))
disp('$\Sigma$')
disp(S)
disp('Maximized LogLikelihood')
disp(llm4)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
