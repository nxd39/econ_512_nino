function [c, ceq]=constraint(par)
S=[par(4), par(5); par(5), par(6)];
c=-(eig(S)-eps.*ones(2, 1));
ceq=[];
end
