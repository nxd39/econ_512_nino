function ll=ll(par, X, Y)
[beta_nodes, weights] = qnwnorm(20, par(1), par(2));
%  these are not the right parameters you are interested in.
k=20;
[~, N] = size(X);
beta=beta_nodes*ones(1, N);
feval=zeros(k, N);
value=zeros(k, N);
for i=1:k
     feval(i, :)=integrand(beta(i,:), X, Y);
     value(i, :)=feval(i, :).*weights(i, 1);
end
integral=sum(value);
lnintegral=log(integral');
ll=-sum(lnintegral);
end